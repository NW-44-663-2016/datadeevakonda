﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.IO;

namespace DataDeevakonda.Models
{
    public class Aquarium
    {
        [ScaffoldColumn(false)]
        public int AquariumID { get; set; }

        [Required]
        public string AquariumName { get; set; }

        [Required]
        [Display(Name = "Entry Ticket Price")]
        public double entryTicketPrice { get; set; }

        [Required]
        [Display(Name = "Child Ticket Price")]
        public double childTicketPrice { get; set; }

        [ScaffoldColumn(false)]
        public int? LocationID { get; set; }

        public virtual Location Location { get; set; }

        public static List<Aquarium> ReadAllFromCSV(string filepath)
        {
            List<Aquarium> lst = File.ReadAllLines(filepath)
                                        .Skip(1)
                                        .Select(v => Aquarium.OneFromCsv(v))
                                        .ToList();
            return lst;
        }

        public static Aquarium OneFromCsv(string csvLine)
        {
            string[] values = csvLine.Split(',');
            Aquarium item = new Aquarium();

            int i = 0;
            item.AquariumName = Convert.ToString(values[i++]);
            item.entryTicketPrice = Convert.ToDouble(values[i++]);
            item.childTicketPrice = Convert.ToDouble(values[i++]);
            item.LocationID = Convert.ToInt32(values[i++]);


            return item;
        }
    }
}
