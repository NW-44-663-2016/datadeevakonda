using System.Linq;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using Microsoft.Data.Entity;
using DataDeevakonda.Models;

namespace DataDeevakonda.Controllers
{
    public class AquariumController : Controller
    {
        private AppDbContext _context;

        public AquariumController(AppDbContext context)
        {
            _context = context;    
        }

        // GET: Aquarium
        public IActionResult Index()
        {
            return View(_context.Aquariums.ToList());
        }

        // GET: Aquarium/Details/5
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Aquarium aquarium = _context.Aquariums.Single(m => m.AquariumID == id);
            if (aquarium == null)
            {
                return HttpNotFound();
            }

            return View(aquarium);
        }

        // GET: Aquarium/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Aquarium/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Aquarium aquarium)
        {
            if (ModelState.IsValid)
            {
                _context.Aquariums.Add(aquarium);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(aquarium);
        }

        // GET: Aquarium/Edit/5
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Aquarium aquarium = _context.Aquariums.Single(m => m.AquariumID == id);
            if (aquarium == null)
            {
                return HttpNotFound();
            }
            return View(aquarium);
        }

        // POST: Aquarium/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(Aquarium aquarium)
        {
            if (ModelState.IsValid)
            {
                _context.Update(aquarium);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(aquarium);
        }

        // GET: Aquarium/Delete/5
        [ActionName("Delete")]
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Aquarium aquarium = _context.Aquariums.Single(m => m.AquariumID == id);
            if (aquarium == null)
            {
                return HttpNotFound();
            }

            return View(aquarium);
        }

        // POST: Aquarium/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            Aquarium aquarium = _context.Aquariums.Single(m => m.AquariumID == id);
            _context.Aquariums.Remove(aquarium);
            _context.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}
